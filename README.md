Swagger Example
===============

```
swagger: '2.0'
info:
  version: 0.0.1
  title: Swagger Example
paths:
  /foo:
    get:
      responses:
        '200':
          description: OK
          schema:
            type: object
            properties:
              Pet:
                type: object
                properties:
                  id:
                    type: integer
                    format: int64
                  name:
                    type: string
              Store:
                type: object
                properties:
                  id:
                    type: integer
                    format: int64
                  name:
                    type: string
  /bar:
    get:
      responses:
        '200':
          description: OK
          schema:
            type: object
            properties:
              id:
                type: integer
                format: int64
              name:
                type: string
definitions:
  User:
    type: object
    properties:
      name:
        type: string
```
